<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "reserva".
 *
 * @property int $id
 * @property int|null $idCliente
 * @property int|null $idPiscina
 * @property string|null $fechaHora
 * @property int|null $duracion
 * @property float|null $precio
 *
 * @property Cliente $idCliente0
 * @property Piscina $idPiscina0
 */
class Reserva extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'reserva';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idCliente', 'idPiscina', 'duracion'], 'integer'],
            [['fechaHora', 'activo'], 'safe'],
            [['precio'], 'number'],
            [['idCliente', 'idPiscina', 'fechaHora'], 'unique', 'targetAttribute' => ['idCliente', 'idPiscina', 'fechaHora']],
            [['idCliente'], 'exist', 'skipOnError' => true, 'targetClass' => Cliente::class, 'targetAttribute' => ['idCliente' => 'id']],
            [['idPiscina'], 'exist', 'skipOnError' => true, 'targetClass' => Piscina::class, 'targetAttribute' => ['idPiscina' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idCliente' => 'Id Cliente',
            'idPiscina' => 'Id Piscina',
            'fechaHora' => 'Fecha Hora',
            'duracion' => 'Duracion',
            'precio' => 'Precio',
        ];
    }

    /**
     * Gets query for [[IdCliente0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdCliente0()
    {
        return $this->hasOne(Cliente::class, ['id' => 'idCliente']);
    }

    /**
     * Gets query for [[IdPiscina0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdPiscina0()
    {
        return $this->hasOne(Piscina::class, ['id' => 'idPiscina']);
    }
}
