<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\User;

use function PHPUnit\Framework\isNull;

class Cliente extends ActiveRecord  implements \yii\web\IdentityInterface
{

    public static function tableName()
    {
        return 'cliente';
    }


    public function rules()
    {
        return [
            [['administrador', 'id'], 'safe'],
            [['nombre', 'contrasena', 'apellidos', 'telefono', 'fechaHora'], 'string', 'max' => 255],
            [['nombre', 'contrasena', 'correo', 'username'], 'required', 'message' => 'El campo {attribute} es obligatorio', 'on' => 'crear'],
            // que el usuario no exista
            [['username', 'correo'], 'unique', 'message' => 'El {attribute} ya existe en el sistema'],
            ['contrasena', 'string', 'min' => 4, 'message' => 'la contraseña debe tener al menos 6 caracteres'],
            ['correo', 'email', 'message' => 'Escribe un correo correctamente'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'nombre' => 'Nombre',
            'contrasena' => 'Contraseña'

        ];
    }


    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }
    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /* public function validatePassword($password)
      {
      return Yii::$app->security->validatePassword($password, $this->password_hash);
      } */

    public function validatePassword($password)
    {
        return Yii::$app->getSecurity()->validatePassword($password, $this->contrasena);
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return true;
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return null;
    }

    public function beforeValidate()
    {
        if (isNull($this->id)) {
            $this->id = Cliente::findBySql('select count(*) from cliente')->scalar() + 1;
        }
        return true;
    }

    public function beforeSave($insert)
    {

        if (parent::beforeSave($insert)) {

            if ($this->isNewRecord) {
                $this->contrasena = Yii::$app->security->generatePasswordHash($this->contrasena);
            } else {
                if (!empty($this->getDirtyAttributes(["contrasena"]))) {
                    $this->contrasena = Yii::$app->security->generatePasswordHash($this->contrasena);
                }
            }

            return true;
        }

        return false;
    }

    /**
     * Comprobamos si el usuario es administrador
     * @param mixed $id
     * @return bool
     */
    public static function isUserAdmin($id)
    {
        if (Cliente::findOne(['id' => $id, 'administrador' => true])) {
            return true;
        } else {

            return false;
        }
    }

    /**
     * Summary of isUserSimple
     * @param mixed $id
     * @return bool
     */
    public static function isUserSimple($id)
    {
        if (Cliente::findOne(['id' => $id, 'administrador' => false])) {
            return true;
        } else {

            return false;
        }
    }
}
