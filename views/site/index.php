<?php

/** @var yii\web\View $this */

use yii\helpers\Html;

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent mt-5 mb-5">
        <h1 class="display-4">Bienvenido a Tu Parque Acuático.com</h1>

        <p class="lead">El parque donde podrás refrescarte y deslizarte por numerosas piscinas en un mundo donde el agua es la protagonista..</p>

        <p><?= Yii::$app->user->isGuest ?  Html::a("Registrate para reservar", ["site/cliente"], ["class" => "btn btn-primary"]) : Html::a("Reservar aqui", ["site/reserva", 'id' => Yii::$app->user->identity->id], ["class" => "btn btn-primary"]) ?></p>

    </div>

</div>