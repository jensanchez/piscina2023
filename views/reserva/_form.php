<?php

use app\models\Piscina;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Reserva $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="reserva-form">

    <?php $form = ActiveForm::begin(); ?>



    <?= $form->field($model, 'idCliente')->textInput() ?>

    <?= $form->field($model, 'idPiscina')->dropDownList(['a', 'b', 'c']) ?>

    <?= $form->field($model, 'fechaHora')->input('datetime-local') ?>

    <?= $form->field($model, 'duracion')->textInput() ?>

    <?= $form->field($model, 'precio')->textInput() ?>
    <?= $form->field($model, 'activo')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>