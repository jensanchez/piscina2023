<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Piscina $model */

$this->title = 'Create Piscina';
$this->params['breadcrumbs'][] = ['label' => 'Piscinas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="piscina-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
