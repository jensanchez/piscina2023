<?php

use yii\bootstrap5\Nav;
use yii\helpers\Html;

echo Nav::widget([
    'options' => ['class' => 'navbar-nav'],
    'items' => [
        ['label' => 'Home', 'url' => ['/site/index']],
        ['label' => 'Registrar', 'url' => ['/site/cliente']],
        ['label' => 'Login', 'url' => ['/site/login']]
    ]
]);
