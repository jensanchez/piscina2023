<?php

use yii\bootstrap5\Nav;
use yii\helpers\Html;


echo Nav::widget([
    'options' => ['class' => 'navbar-nav'],
    'items' => [
        ['label' => 'Inicio', 'url' => ['/site/index']],
        ['label' => 'Cliente Reserva', 'url' => ['/site/reserva', 'id' => Yii::$app->user->identity->id]],
        ['label' => 'Contacto', 'url' => ['/site/contact']],
        '<li class="nav-item">'
            . Html::beginForm(['/site/logout'])
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->username . ')',
                ['class' => 'nav-link btn btn-link logout']
            )
            . Html::endForm()
            . '</li>'
    ]
]);
