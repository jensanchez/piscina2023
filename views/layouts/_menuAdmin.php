<?php

use yii\bootstrap5\Nav;
use yii\helpers\Html;


echo Nav::widget([
    'options' => ['class' => 'navbar-nav'],
    'items' => [
        ['label' => 'Inicio', 'url' => ['/site/index']],
        ['label' => 'Contacto', 'url' => ['/site/contact']],
        ['label' => 'Cliente', 'url' => ['/cliente/index']],
        ['label' => 'Piscina', 'url' => ['/piscina/index']],
        ['label' => 'Reserva', 'url' => ['/reserva/index']],
        '<li class="nav-item">'
            . Html::beginForm(['/site/logout'])
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->username . ')',
                ['class' => 'nav-link btn btn-link logout']
            )
            . Html::endForm()
            . '</li>'
    ]
]);
