﻿DROP DATABASE IF EXISTS piscina2023;
CREATE DATABASE piscina2023;
USE piscina2023;

CREATE TABLE cliente(
id int,
nombre varchar(300),
apellidos varchar(300),
correo varchar(200) NOT NULL,
telefono varchar(100)NOT NULL,
fechaHora datetime,
contrasena varchar(100),
PRIMARY KEY(id),
UNIQUE KEY(correo)
);

CREATE TABLE piscina(
id int,
nombre varchar(300),
descripcion text,
aforo int,
PRIMARY KEY(id)
);

CREATE TABLE reserva(
id int,
idCliente int,
idPiscina int,
fechaHora datetime,
duracion int,
precio float,
PRIMARY KEY(id),
UNIQUE KEY(idCliente,idPiscina,fechaHora)
);

ALTER TABLE reserva 
  ADD CONSTRAINT fkReservaCliente FOREIGN KEY(idCliente) REFERENCES cliente(id),
  ADD CONSTRAINT fkReservaPiscina FOREIGN KEY(idPiscina) REFERENCES piscina(id);

